#ifndef COMMANDS_H
#define COMMANDS_H

#include <stdint.h>
#include <cstdlib>
#include "registers.h"
#include "UART.h"
#include "timer.h"
#include "ADC.h"
#include "control.h"

//Limit the command length and define length of command list
#define COMMAND_LENGTH 20
#define COMMAND_LIST_LENGTH 13

//Set up command memory
#define COMMAND_BUFFER_LENGTH 10

//Command history buffer
static char COMMAND_MEMORY[COMMAND_BUFFER_LENGTH][COMMAND_LENGTH];

//These stings are compared against the imput string to find a matching function call
static const char COMMANDLIST[COMMAND_LIST_LENGTH][COMMAND_LENGTH] = {
	"!",
	"HISTORY",
	"LED ON ",
	"LED OFF ",
	"HELP",
	"QUERY LED ",
	"INFO",
	"SET ANGLE ",
	"VALVE OFF",
	"VALVE ON",
	"ADC READ",
	"CONTROL ON ",
	"CONTROL OFF"
};

void BANG_FUNCTION (char * data);
void HISTORY (char * data);
void LED_ON (char * LED);
void LED_OFF (char * LED);
void HELP (char * data);
void QUERY_LED (char * LED);
void INFO (char * data);
void SETANGLE (char * data);
void VALVEOFF (char * data);
void VALVEON (char * data);
void ADCREAD (char * data);
void ERROR (char * data);
void CONTROL_ON (char * data);
void CONTROL_OFF (char * data);

//Helper functions
int StringToInt (char * data);
void SendCommand(char * command, uint8_t addBuf);

//This list of functions corresponds to the above list of match strings
static void (*functionCalls[COMMAND_LIST_LENGTH])(char * data) = {
	BANG_FUNCTION,
	HISTORY,
	LED_ON,
	LED_OFF,
	HELP,
	QUERY_LED,
	INFO,
	SETANGLE,
	VALVEOFF,
	VALVEON,
	ADCREAD,
	CONTROL_ON,
	CONTROL_OFF
};
#endif
