#include "commands.h"

void SendCommand (char * command, uint8_t addBuf) {
	//Execute command
	char data[COMMAND_LENGTH];
	int c, d, i, j, k, l;
	int dataNum = 0;
	
	//First, take out any pairs of whitespaces
	for (c = 0; c < (COMMAND_LENGTH - 1) && command[c] != 0; c += 1) {
		if (command[c] == ' ' && command[c+1] == ' ') {//If 2 whitespaces...
			for (d = c; d < (COMMAND_LENGTH - 1) && command[d] != 0; d += 1) {
				command[d] = command[d+1];//...shift everything after the first space left by one
			}
			c -= 1;//Checks for more than 2 whitespaces
		}
	}
	
	for (i = 0; i < COMMAND_LIST_LENGTH; i += 1) {
		for (j = 0; (j < COMMAND_LENGTH) && ((COMMANDLIST[i][j] == command[j]) || (COMMANDLIST[i][j] == 0x0)); j += 1) {
			//Look for complete match, get any data
			if (COMMANDLIST[i][j] == 0x0) {
				data[dataNum] = command[j];
				dataNum += 1;
			}
		}
		//Everything else is data
		if (j == COMMAND_LENGTH) {
			//Command found, execute command and add to command history...
			functionCalls[i](data);
			//...but only if not the "!" command
			if(addBuf && i != 0 && i != 1) {
				//If the command isn't "!" or "HISTORY", and the addBuf flag is true, add the command to the history
				for(k = COMMAND_BUFFER_LENGTH - 1; k > 0; k -= 1) {
					//Shift commands up in the buffer
					for (l = 0; l < COMMAND_LENGTH; l += 1) {
						COMMAND_MEMORY[k][l] = COMMAND_MEMORY[k-1][l];
					}
				}
				//Copy command to lowest location in buffer
				for (k = 0; k < COMMAND_LENGTH; k += 1) {
					COMMAND_MEMORY[0][k] = command[k];
				}
			}
			break;
		}
	}
	
	//If nothing found, print an error
	if (i == COMMAND_LIST_LENGTH) {
		ERROR(command);
	}
}

//Repeat previous function calls
void BANG_FUNCTION (char * data) {
	int bufferLoc;
	if (data[0] == 0) {//"!" sent with no arguments
		//Default to location 0
		bufferLoc = 0;
	} else {//"!X" sent where X is a number
		bufferLoc = StringToInt(data);
	}
	if(bufferLoc < 0 || bufferLoc >= COMMAND_BUFFER_LENGTH) {//Make sure we don't go outside buffer
		SendLine("Bad input, unable to parse arguments: ");
		SendLine(data);
		SendLine("\r\n");
		return;
	} else {
		//Send command, but don't add the bang to the command buffer
		SendCommand(COMMAND_MEMORY[bufferLoc], 0);
	}
}

//Print the contents of the command history buffer
void HISTORY (char * data) {
	int i;
	for(i = COMMAND_BUFFER_LENGTH - 1; i >= 0; i -= 1) {
		if (COMMAND_MEMORY[i][0] != 0) {//Only print non-empty commands
			SendInt(i);
			SendLine(": ");
			SendLine(COMMAND_MEMORY[i]);
			SendLine("\r\n");
		}
	}
}

//Turn on some LEDs
void LED_ON (char * LED) {
	//Turn on an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {//Turn on all LEDs
		GPIOB_ODR |= 0xFF00;
	} else {
		GPIOB_ODR |= 1 << (LED[0] - 41);
	}
}

//Turn off some LEDs
void LED_OFF (char * LED) {
	//Turn off an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {
		GPIOB_ODR &= ~(0xFF00);
	} else {
		GPIOB_ODR &= ~(1 << (LED[0]-41));
	}
}

//Describe commands
void HELP (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("This is the help menu\r\n");
	SendLine("\"HISTORY\" displays the previous commands indexed in order\r\n");
	SendLine("\"!\" performs the previous command at index 0\r\n");
	SendLine("\"!X\" performs previous command at index X\r\n");
	SendLine("\"LED ON X\" will turn on LED X\r\n");
	SendLine("\"LED OFF X\" will turn off LED X\r\n");
	SendLine("\"LED ON 0\" will turn on all LEDs\r\n");
	SendLine("\"LED OFF 0\" will turn off all LEDs\r\n");
	SendLine("\"QUERY LED X\" will report the status of LED X\r\n");
	SendLine("\"SET ANGLE XYZ\" sets the angle of the valve to the number XYZ\r\n");
	SendLine("\"VALVE ON\" turns valve controls on\r\n");
	SendLine("\"VALVE OFF\" turns valve controls off\r\n");
	SendLine("\"ADC READ\" prints the value of the ADC\r\n");
	SendLine("\"CONTROL ON ABCD\" turns on the automated valve controls and\r\n");
	SendLine("tries to hold the pong ball at AB.CD cm (ABCD is the desired\r\n");
	SendLine("height * 100)\r\n");
	SendLine("\"CONTROL OFF\" turns off the automated valve controls\r\n");
	SendLine("\"INFO\" prints date and time of compilation\r\n");
	SendLine("\"HELP\" prints this help message\r\n");
}

//Return state of LED
void QUERY_LED (char * LED) {
	if (LED[1] != 0 || LED[0] < '1' || LED[0] > '8') {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (GPIOB_IDR & (1 << (LED[0]-41))) {
		SendLine("The LED is on\r\n");
	} else {
		SendLine("The LED is off\r\n");
	}
}

//Set angle of valve on equipment
void SETANGLE (char * data) {
	int percent = StringToInt(data);
	
	if(percent <= 10000 && percent != -1)
	{
		PWMAngleSet(percent);
		SendLine("Angle set to ");
		SendInt(percent);
		SendLine("%\r\n");
	} else {
		SendLine("Invalid value entered: ");
		SendLine(data);
		SendLine("\r\n");
	}
}

//Enable valve system
void VALVEON (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStart();
	SendLine("Timer started\r\n");
}

//Disable valve system
void VALVEOFF (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStop();
	SendLine("Timer stopped\r\n");
}

//Print the ADC value
void ADCREAD(char * data) {
	SendLine("Current ADC value: ");
	StartADC();
	SendInt(GetADC());
	SendLine("\r\n");
}

//Convert to positive int or negative 1 if not valid
int StringToInt (char * data) {
	int i = 0;//Count variable
	unsigned accumulator = 0;
	if (data[0] == 0) {//Empty string
		return -1;
	}
	
	while (data[i] != 0) {
		if(data[i] < '0' || data[i] > '9') {
			return -1;//Bad character
		} else {
			//Accumulate the value
			accumulator = accumulator * 10 + data[i] - 48;
		}
		i += 1;
	}
	return accumulator;
}

//Display date and time of compilation
void INFO (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("Compilation time and date:\r\n");
	SendLine(__TIME__);
	SendLine("\r\n");
	SendLine(__DATE__);
	SendLine("\r\n");
}

void ERROR (char * data) {
	SendLine("Error: invalid command ");
	SendLine(data);
	SendLine("\r\n");
}

//Enable necessary control structures
void CONTROL_ON (char * data) {
	int temp = StringToInt(data);
	if (temp < 0 ) {
		SendLine("Error: bad argument ");
		SendLine(data);
		SendLine("\r\n");
	}
	//Enable interrupt, set the setpoint, and start conversion
	SETPOINT = (unsigned) temp;
	EnableADCInt();
	StartADC();
}

void CONTROL_OFF (char * data) {
	DisableADCInt();
}
